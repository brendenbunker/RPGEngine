﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using RPGEngine.JsonImporters.Dictionary;

namespace JsonImporters
{
    [ContentProcessor(DisplayName = "Object Dictionary Processor")]
    class ObjectDictionaryProcessor : ContentProcessor<ObjectDictionary, ObjectDictionary>
    {
        public override ObjectDictionary Process(ObjectDictionary input, ContentProcessorContext context)
        {
            return input;
        }
    }
}

