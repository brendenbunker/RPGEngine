﻿using RPGEngine.Engine;
using RPGEngine.Entities.Character;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace RPGEngine.Entities.Character
{
    class Player1 : Actor
    {
        private float rad;
        private static int playerNum = 1;

        public Player1(int x, int y, float radius) : base(x, y, CharacterType.Player)
        {
            rad = radius;
            tileWidth = (int)rad * 2;
            tileHeight = (int)rad * 2;
            this.movementSpeed = 95;
            properties = new List<object>();
            properties.Add(playerNum++);
        }

        public override void loadContent(ContentManager content)
        {
            texture = content.Load<Texture2D>("textures/Face_" + properties[0]);
        }

        public override void update()
        {
            if (transLength > 0)
            {
                switch (transDirection)
                {
                    case (Direction.Up):
                        if (RPGEngine.correctY && RPGEngine.camera.Position.Y > 0 && y * tileHeight - RPGEngine.camera.Position.Y < RPGEngine.viewport.Height/2)
                        {
                            RPGEngine.camera.Move(new Vector2(0, -transtionRate));
                        }
                        transOffsetY -= transtionRate;
                        break;

                    case (Direction.Down):
                        if (RPGEngine.correctY && RPGEngine.camera.Position.Y + RPGEngine.viewport.Height < RPGEngine.mapBounds.Height && RPGEngine.camera.WorldToScreen(new Vector2(x * tileWidth, y * tileHeight)).Y > RPGEngine.viewport.Height/2)
                        {
                            RPGEngine.camera.Move(new Vector2(0, transtionRate));
                        }
                        transOffsetY += transtionRate;
                        break;

                    case (Direction.Left):
                        if (RPGEngine.correctX && RPGEngine.camera.Position.X > 0 && x * tileWidth - RPGEngine.camera.Position.X < RPGEngine.viewport.Width / 2)
                        {
                           RPGEngine.camera.Move(new Vector2(-transtionRate, 0));
                        }
                        transOffsetX -= transtionRate;
                        break;

                    case (Direction.Right):
                        if (RPGEngine.correctX && RPGEngine.camera.Position.X + RPGEngine.viewport.Width < RPGEngine.mapBounds.Width && RPGEngine.camera.WorldToScreen(new Vector2(x * tileWidth, y * tileHeight)).X > RPGEngine.viewport.Width / 2)
                        {
                            RPGEngine.camera.Move(new Vector2(transtionRate, 0));
                        }
                        transOffsetX += transtionRate;
                        break;
                }

                transLength--;
                if (transLength == 0)
                {
                    transOffsetX = 0;
                    transOffsetY = 0;
                    switch (transDirection)
                    {
                        case (Direction.Up):
                            y--;
                            break;
                        case (Direction.Down):
                            y++;
                            break;
                        case (Direction.Left):
                            x--;
                            break;
                        case (Direction.Right):
                            x++;
                            break;
                    }
                    transDirection = null;
                }
            }
        }

        public override void render(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(texture, new Vector2((this.x*32) + rad + transOffsetX, (this.y * 32) + rad + transOffsetY), null, Color.White, 0f, texture.Bounds.Center.ToVector2(), 1, SpriteEffects.None, 0f);

            //Old render method, Does not rotate.

            /*bounds = ((PolygonShape)body.FixtureList[0].Shape);
            List<Vector2> verts = bounds.Vertices.ToList();
            LinkedList<Vector2> newVerts = new LinkedList<Vector2>();
            foreach (var vert in verts)
            {
                newVerts.AddLast(new Vector2(FarseerPhysics.ConvertUnits.ToDisplayUnits(vert.X), FarseerPhysics.ConvertUnits.ToDisplayUnits(vert.Y)));
            }

            test = new PolygonF(newVerts);
            spriteBatch.DrawPolygon(FarseerPhysics.ConvertUnits.ToDisplayUnits(body.Position), test, Color.White);*/

        }

        public override void activateAbility()
        {

        }
    }
}
