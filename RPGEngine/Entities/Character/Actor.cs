﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Shapes;

namespace RPGEngine.Entities.Character
{
    enum CharacterType
    {
        Player
    };

    abstract class Actor : Entity
    {
        public Texture2D texture;
        public CharacterType type;
        public int movementSpeed;
        public float transLength;
        public float transOffsetX = 0, transOffsetY = 0;
        public float transtionRate = 0;
        public Direction? transDirection;
        public List<object> properties;

        public Actor(int x, int y, CharacterType type) : base(x, y)
        {
            this.type = type;
            transDirection = null;
        }


        public abstract void activateAbility();

        public abstract void loadContent(ContentManager content);
    }
}
