﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace RPGEngine.Entities.Items
{
    enum ItemType
    {

    };
    abstract class Item : Entity
    {
        private ItemType type;
        public Item(int x, int y, ItemType type) : base(x, y)
        {
            this.type = type;
        }

    }
}
