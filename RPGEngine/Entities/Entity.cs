﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RPGEngine.Entities
{
    abstract class Entity
    {
        public float x, y, tileWidth, tileHeight;
        public Entity(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        abstract public void update();
        abstract public void render(SpriteBatch spriteBatch);
    }
}
