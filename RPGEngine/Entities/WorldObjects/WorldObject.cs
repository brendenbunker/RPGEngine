﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;
using RPGEngine.JsonImporters.Dictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine.Entities.WorldObjects
{
    class WorldObject : Entity
    {
        public ObjectType? type;
        public int tilesetID;
        public TiledMapTileset tileset;
        public bool isSolid;

        public WorldObject(int x, int y, bool isSolid, MonoGame.Extended.Tiled.TiledMapTileset tileset, int position, ObjectType? type, int width, int height) : base(x, y)
        {
            this.isSolid = isSolid;
            this.type = type;
            this.tileWidth = width;
            this.tileHeight = height;
            this.tileset = tileset;
            this.tilesetID = position - 1;
        }

        public override void render(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(tileset.Texture, new Vector2(x*tileWidth, y*tileHeight), new Rectangle(tileset.TileWidth * tilesetID, 0, (int)tileWidth, (int)tileHeight), Color.White);
        }

        public override void update()
        {

        }
    }
}
