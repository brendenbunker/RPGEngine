﻿namespace RPGEngine.Entities.Enemies
{
    enum EnemyType
    {
        
    };
    abstract class Enemy : Entity
    {
        public EnemyType type;
        public Enemy(int x, int y, EnemyType type) : base(x, y)
        {
            this.type = type;
        }
    }
}
