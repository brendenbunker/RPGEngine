﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGEngine.JsonImporters.Dictionary
{
    public class ObjectDictionary
    {
        public List<ObjectDefinition> objectDictionary { get; set; }
    }

    public enum ObjectType
    {
        Wall,
        Ground,
        NPC
    };
}
