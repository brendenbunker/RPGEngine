﻿using Microsoft.Xna.Framework.Content; 
using Newtonsoft.Json;
using System.IO;
using RPGEngine.JsonImporters.Dictionary;

namespace JsonImporters
{
    [ContentImporter(".rnod", DisplayName = "RPG Engine Object Dictionary", DefaultProcessor = "ObjectDictionaryProcessor")]
    public class ObjectDicionaryImporter : ContentImporter<ObjectDictionary>
    {
        public override ObjectDictionary Import(string filename, ContentImporterContext context)
        {
            context.Logger.LogMessage("Importing ObjectDictionary file: {0}", filename);

            using (StreamReader file = File.OpenText(filename))
            {
                JsonSerializer serializer = new JsonSerializer();
                ObjectDictionary list = (ObjectDictionary)serializer.Deserialize(file, typeof(ObjectDictionary));
                return list;
            }
        }
    }
}

