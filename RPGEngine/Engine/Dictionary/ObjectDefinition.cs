﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGEngine.JsonImporters.Dictionary
{
    public class ObjectDefinition
    {
        public int ID { get; set; }
        public string objectType { get; set; }
        public bool isSolid { get; set; }
        public string tilesheet { get; set; }
        public string tilePostion { get; set; }
        public string name { get; set; } 
    }
}
