﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGEngine
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
}
