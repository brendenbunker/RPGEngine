﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine.Engine.RenderHelpers
{
    public class OverlayManager
    {
        public List<OverlayItem> gui;
        public SpriteFont font;
        public OverlayManager()
        {
            gui = new List<OverlayItem>();
        }

        public void addContainer()
        {

        }

        public void render(SpriteBatch spriteBatch)
        {  
            foreach (var item in gui)
            {
                item.render(spriteBatch);
            }
        }
    }
}
