﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RPGEngine.Engine.RenderHelpers
{
    class OverlayText : OverlayItem
    {
        public string text;
        public OverlayText(Color color) : base()
        {
            this.padding = 2;
            this.color = color;
            text = "";
        }

        public override float getWidth()
        {
            float width = padding * 2;
            width += RPGEngine.overlay.font.MeasureString(text).X;
            return width;
        }

        public override float getHeight()
        {
            float height = padding * 2;
            height += RPGEngine.overlay.font.MeasureString(text).Y;
            return height;
        }

        public override void render(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(RPGEngine.overlay.font, text, new Vector2(x, y), color);
        }

        public override float getSumWidth()
        {
            return getWidth();
        }

        public override float getSumHeight()
        {
            return getHeight();
        }
    }
}
