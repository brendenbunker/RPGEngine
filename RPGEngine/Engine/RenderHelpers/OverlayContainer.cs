﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGEngine.Engine.RenderHelpers
{
    public enum Distribution
    {
        Vertical,
        Horizontal
    }

    public class OverlayContainer:OverlayItem
    {
        List<OverlayItem> contained;
        Distribution distribution;
        public OverlayContainer(float x, float y, float width, float height, Color color, Distribution distribution)
        {
            this.distribution = distribution;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.color = color;
            contained = new List<OverlayItem>();
        }

        public OverlayContainer addOverlayItem(OverlayItem item)
        {
            contained.Add(item);
            return this;
        }

        public void updateDistribution()
        {
            if (distribution == Distribution.Horizontal)
            {
                if (getWidth() > this.width)
                {
                    this.width = getWidth() + padding * 2;
                }

                float gapSpace = (width - getWidth()) / (contained.Count + 1);
                float currentOffset = this.x;
                foreach (var item in contained)
                {
                    if (this.height < item.getHeight())
                    {
                        this.height = item.getHeight() + (padding * 2);
                    }

                    currentOffset += gapSpace;
                    item.x = currentOffset;
                    currentOffset += item.getSumWidth();
                    item.y = this.y;
                }
            }

            if (distribution == Distribution.Vertical)
            {
                if (getHeight() > this.height)
                {
                    this.height = getHeight() + padding * 2;
                }
                float gapSpace = (height - getHeight()) / (contained.Count + 1);
                float currentOffset = this.y;
                foreach (var item in contained)
                {

                    if (this.width < item.getWidth())
                    {
                        this.width = item.getWidth() + (padding * 2);
                    }

                    currentOffset += gapSpace;
                    item.y = currentOffset;
                    currentOffset += item.getSumHeight();
                    item.x = this.x;
                }
            }
        }

        public override void render(SpriteBatch spriteBatch)
        {
            updateDistribution();

            spriteBatch.FillRectangle(new RectangleF(x,y,width,height), color);
            foreach (var item in contained)
            {
                item.render(spriteBatch);
            }
        }

        public override float getWidth()
        {
            return this.width;
        }

        public override float getHeight()
        {
            return this.height;
        }

        public override float getSumWidth()
        {
            float objectsWidth = 0;
            foreach (var item in contained)
            {
                objectsWidth += item.getWidth();
            }

            return objectsWidth;
        }

        public override float getSumHeight()
        {
            float objectsHeight = 0;
            foreach (var item in contained)
            {
                objectsHeight += item.getHeight();
            }
            return objectsHeight;
        }
    }
}
