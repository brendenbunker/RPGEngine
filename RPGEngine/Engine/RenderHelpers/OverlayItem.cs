﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEngine.Engine.RenderHelpers
{
    public abstract class OverlayItem
    {
        public static bool updateOverlay;
        public float x, y, width, height, padding;
        public Color color;
        public OverlayItem()
        {

        }

        public abstract void render(SpriteBatch spriteBatch);
        public abstract float getWidth();
        public abstract float getHeight();
        public abstract float getSumWidth();
        public abstract float getSumHeight();
    }
}
