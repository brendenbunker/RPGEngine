﻿using RPGEngine.Entities.Character;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using RPGEngine.Engine.RenderHelpers;
using com.bitbull.meat;

namespace RPGEngine.Engine.Player
{
    class Player
    {

        public Actor actor;
        public OverlayText playerPos, playerNum, LerpTimer;
        bool changed = false;
        Vector2 oldActorPoint;
        public int LerpToNewActor = 0;


        public Player(Actor character)
        {
            this.actor = character;
            this.playerPos = new OverlayText(Color.White);
            this.playerNum = new OverlayText(Color.White);
            this.LerpTimer = new OverlayText(Color.White);

            oldActorPoint = new Vector2(this.actor.x, this.actor.y);
            RPGEngine.overlay.gui.Add(
                new OverlayContainer(0, RPGEngine.viewport.Height, 200, 0, Color.Purple , Distribution.Vertical).addOverlayItem(
                    new OverlayContainer(300,RPGEngine.viewport.Height,200,20,Color.Red,Distribution.Horizontal).addOverlayItem(playerNum)).addOverlayItem(
                    new OverlayContainer(0, RPGEngine.viewport.Height, 200, 20, Color.Pink, Distribution.Horizontal).addOverlayItem(playerPos).addOverlayItem(LerpTimer)));

            //RPGEngine.overlay.gui.Add(
            //new OverlayContainer(0, RPGEngine.viewport.Height, 200, 0, Color.Purple, Distribution.Vertical).addOverlayItem(
                //new OverlayContainer(300, RPGEngine.viewport.Height, 200, 20, Color.Red, Distribution.Horizontal).addOverlayItem(playerNum)).addOverlayItem(
                //new OverlayContainer(0, RPGEngine.viewport.Height, 200, 20, Color.Pink, Distribution.Horizontal).addOverlayItem(
                    //new OverlayContainer(0, RPGEngine.viewport.Height, 100, 20, Color.Blue, Distribution.Horizontal).addOverlayItem(playerPos)).addOverlayItem(
                    //new OverlayContainer(0, RPGEngine.viewport.Height, 100, 20, Color.ForestGreen, Distribution.Horizontal).addOverlayItem(LerpTimer))));
        }

        public void update(Storage storage)
        {
            KeyboardState state = Keyboard.GetState();
            if(state.IsKeyDown(Keys.W) && actor.transLength==0){
                if (!storage.getWorldObjectAtPos(actor.x, actor.y - 1).isSolid)
                {
                    StartTranstion(Direction.Up, actor.tileHeight);
                }
            }

            if (state.IsKeyDown(Keys.S) && actor.transLength == 0)
            {
                if (!storage.getWorldObjectAtPos(actor.x, actor.y + 1).isSolid)
                {
                    StartTranstion(Direction.Down, actor.tileHeight);
                }
            }

            if (state.IsKeyDown(Keys.A) && actor.transLength == 0)
            {
                if (!storage.getWorldObjectAtPos(actor.x - 1, actor.y).isSolid)
                {
                    StartTranstion(Direction.Left, actor.tileWidth);
                }    
            }

            if (state.IsKeyDown(Keys.D) && actor.transLength == 0)
            {
                if (!storage.getWorldObjectAtPos(actor.x + 1, actor.y).isSolid)
                {
                    StartTranstion(Direction.Right, actor.tileWidth);
                }
            }

            if (state.IsKeyDown(Keys.Space) && !changed)
            {
                changed = true;
                int currInd = storage.actors.FindIndex(a => a.properties[0] == actor.properties[0]);
                if (++currInd >= storage.actors.Count)
                {
                    currInd = 0;
                }
                setPlayerTarget(storage.actors[currInd]);
            }
            else if(state.IsKeyUp(Keys.Space))
            {
                changed = false;
            }

            playerPos.text = actor.x + ", " + actor.y;
            playerNum.text = actor.type.ToString() + " " + actor.properties[0];
            LerpTimer.text = LerpToNewActor + "";
            
        }

        public void StartTranstion(Direction direction, float distance)
        {
            actor.transLength = (100 - actor.movementSpeed);
            actor.transDirection = direction;
            actor.transtionRate = distance / actor.transLength;
        }

        public void setPlayerTarget(Actor actor)
        {
            oldActorPoint = RPGEngine.camera.Position;
            this.actor = actor;
            LerpToNewActor = 30;
        }
    }
}
