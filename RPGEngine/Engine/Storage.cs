﻿using RPGEngine.Entities.Character;
using RPGEngine.Entities.Enemies;
using RPGEngine.Entities.Items;
using RPGEngine.Entities.WorldObjects;
using System.Collections.Generic;

namespace RPGEngine.Engine
{
    class Storage
    {
        public List<Enemy> enemies;
        public List<Player.Player> players;
        public List<Item> items;
        public List<WorldObject> worldObjects;
        public List<Actor> actors;
        public int mapHeight, mapWidth;
        public int tileWidth;
        public int tileHeight;
        public Player.Player activePlayer;

        public Storage(int mapWidth, int mapHeight, int tileWidth, int tileHeight)
        {
            enemies = new List<Enemy>();
            players = new List<Player.Player>();
            items = new List<Item>();
            worldObjects = new List<WorldObject>();
            actors = new List<Actor>();

            this.mapWidth = mapWidth;
            this.mapHeight = mapHeight;
            this.tileWidth = tileWidth;
            this.tileHeight = tileHeight;
        }

        public WorldObject getWorldObjectAtPos(int x, int y)
        {
            int index = this.mapWidth * y + x;
            return worldObjects[index];
        }

        public WorldObject getWorldObjectAtPos(float x, float y)
        {
            int index = (int)(this.mapWidth * y + x);
            return worldObjects[index];
        }
    }
}
