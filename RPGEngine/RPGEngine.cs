﻿using com.bitbull.meat;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.Tiled;
using MonoGame.Extended.ViewportAdapters;
using RPGEngine.Engine;
using RPGEngine.Engine.RenderHelpers;
using RPGEngine.Entities.Character;
using RPGEngine.Entities.WorldObjects;
using RPGEngine.JsonImporters.Dictionary;
using System;

namespace RPGEngine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class RPGEngine : Game
    {

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static Rectangle viewport, mapBounds;
        int currentRes = 2;
        bool cameraCorrect = false;
        public static bool correctX, correctY;
        public static OverlayManager overlay;
        Lerper lerper;
        Storage storage;
        public static Camera2D camera;

        public RPGEngine()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            storage = new Storage(0, 0, 0, 0);
            overlay = new OverlayManager();
            lerper = new Lerper();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();

            var viewportAdapter = new BoxingViewportAdapter(Window, GraphicsDevice, 1280, 720);
            camera = new Camera2D(viewportAdapter);

            this.Window.Title = "RPGEngine";
            updateResolution(1280, 720);
        }

        public void updateResolution(int width, int height)
        {
            graphics.PreferredBackBufferWidth = width;  // set this value to the desired width of your window
            graphics.PreferredBackBufferHeight = height;   // set this value to the desired height of your window
            graphics.ApplyChanges();
            viewport = graphics.GraphicsDevice.Viewport.Bounds;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //load map
            overlay.font = Content.Load<SpriteFont>("Gui/Fonts/RussoOne18");
            storage.actors.Add(new Player1(5, 1, 16));
            storage.actors.Add(new Player1(3, 1, 16));
            storage.actors.Add(new Player1(1, 1, 16));
            storage.activePlayer = new Engine.Player.Player(storage.actors[storage.actors.Count - 1]);
            storage.players.Add(storage.activePlayer);
            loadMap("CollisionTests");

            //load shit in map
            foreach (var actor in storage.actors)
            {
                actor.loadContent(Content);
            }
            //TODO: use this.Content to load your game content here
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public void loadMap(string mapName)
        {
            TiledMap map = Content.Load<TiledMap>("Maps/" + mapName);
            //ObjectDictionary dictionary = Content.Load<ObjectDictionary>("Dictionary/" + mapName);
            Storage retStor = new Storage(map.Width,map.Height,map.TileWidth,map.TileHeight);
            int currInd = 0;
            foreach (var item in map.TileLayers[0].Tiles)
            {
                //ObjectDefinition def = dictionary.objectDictionary[item.GlobalIdentifier-1];
                //calc x and y
                int itemX = (currInd % map.Width);
                int itemY = (currInd/map.Width);
                ObjectType? type = null;
                if (item.GlobalIdentifier - 1 == 0)
                {
                    type = ObjectType.Ground;
                }
                else if(item.GlobalIdentifier - 1 == 0)
                {
                    type = ObjectType.Wall;
                }
                WorldObject temp = new WorldObject(itemX, itemY, true, map.Tilesets[0], item.GlobalIdentifier, type, map.TileWidth, map.TileHeight);
                retStor.worldObjects.Add(temp);

                currInd++;
            }

            retStor.players = storage.players;
            retStor.actors = storage.actors;
            retStor.activePlayer = storage.activePlayer;
            storage = retStor;
            mapBounds = new Rectangle(0, 0, map.Width * map.TileWidth, map.Height * map.TileHeight);
            cameraCorrect = false;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // For Mobile devices, this logic will close the Game when the Back button is pressed
            // Exit() is obsolete on iOS
#if !__IOS__ && !__TVOS__
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
#endif
            //Change Res on the Fly
            if (Keyboard.GetState().IsKeyDown(Keys.D1) && currentRes != 1)
            {
                //updateResolution(640, 480);
                currentRes = 1;
                loadMap("CollisionTestsAlt");
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.D2) && currentRes != 2)
            {
                //updateResolution(1280, 720);
                currentRes = 2;
                loadMap("CollisionTests");
            }

            //update all objects
            foreach (var player in storage.players)
            {
                player.update(storage);
            }

            foreach (var actor in storage.actors)
            {
                actor.update();
            }

            foreach (var enemy in storage.enemies)
            {
                enemy.update();
            }

            foreach (var item in storage.items)
            {
                item.update();
            }

            foreach (var wObject in storage.worldObjects)
            {
                wObject.update();
            }
            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (!cameraCorrect)
            {
                if(storage.mapWidth * storage.tileWidth <= viewport.Width)
                {
                    camera.Position = new Vector2(-(viewport.Width / 2 - storage.mapWidth * storage.tileWidth / 2), camera.Position.Y);
                    correctX = false;
                }
                else
                {
                    correctX = true;
                }

                if (storage.mapHeight * storage.tileHeight <= viewport.Height)
                {
                    camera.Position = new Vector2(camera.Position.X, -(viewport.Height / 2 - storage.mapHeight * storage.tileHeight / 2));
                    correctY = false;
                }
                else
                {
                    correctY = true;
                }
            }
            GraphicsDevice.Clear(Color.Black);

            var transformMatrix = camera.GetViewMatrix();
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, transformMatrix: transformMatrix);

            // TODO: Add your drawing code here
            //camera.Position = new Vector2(lerper.Lerp(camera.Position.X, (storage.activePlayer.actor.x*storage.activePlayer.actor.tileWidth)-viewport.Width/2), lerper.Lerp(camera.Position.Y, (storage.activePlayer.actor.y * storage.activePlayer.actor.tileHeight) - viewport.Height / 2));
            if (storage.activePlayer.LerpToNewActor > 0) {
                camera.Position = new Vector2(MathHelper.Lerp(camera.Position.X, (storage.activePlayer.actor.x * storage.activePlayer.actor.tileWidth) + storage.activePlayer.actor.transOffsetX - viewport.Width / 2, .05f), MathHelper.Lerp(camera.Position.Y, (storage.activePlayer.actor.y * storage.activePlayer.actor.tileHeight) + storage.activePlayer.actor.transOffsetY - viewport.Height / 2, .05f));
                storage.activePlayer.LerpToNewActor--;
            }

            foreach (var wObject in storage.worldObjects)
            {
                wObject.render(spriteBatch);
            }

            foreach (var item in storage.items)
            {
                item.render(spriteBatch);
            }

            foreach (var enemy in storage.enemies)
            {
                enemy.render(spriteBatch);
            }

            foreach (var actor in storage.actors)
            {
                actor.render(spriteBatch);
            }

            spriteBatch.End();

            //Draw Static Items like Gui
            spriteBatch.Begin();

            overlay.render(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
